---
title: "Hello!"
description: "My name is Tegan and I am an inclusive frontend designer, developer and maker living in the beautiful city of Cardiff in Wales, spreading the joy of accessible and semantic code at a digital agency in the Bay."
---
