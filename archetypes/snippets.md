---
title: "{{ replace .Name "-" " " | title }}"
description: "A great description"
publishDate: {{ .Date }}
slug: a-shorter-slug
source:
  title: Page title
  url: Page URL
tags:
 - command line
 - css
 - git
 - html
 - javascript
---
